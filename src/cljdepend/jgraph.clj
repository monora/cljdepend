(ns ^{:doc
      "Defines JDepGraph, a loom DiGraph where nodes a java types."}

  cljdepend.jgraph
  (:require [loom.graph :refer [nodes edges out-degree
                                graph? directed? has-node? has-edge?
                                successors predecessors
                                add-nodes* add-edges*
                                digraph graph Graph Digraph
                                fly-graph]]
            [loom.derived :refer :all]
            [loom.io :refer [view dot]]
            [clojure.string :refer [join split]])
  (:use [cljdepend.jtype] :reload))

(defn package
  "Return package of java typename"
  [^clojure.lang.Symbol typename]
  (join "." (butlast (split (str typename) #"\."))))

(defn- nth-part-of-package
  [packagename n]
  (let [parts (split packagename #"\.")]
    (if (< n (count parts))
      (nth parts n)
      "unknown")))

(defn subsystem
  "Return subsystem of package"
  [packagename]
  (nth-part-of-package packagename 3))

(defn layer
  "Return layer of package"
  [packagename]
  (nth-part-of-package packagename 4))

(defn to-graph [flygraph]
  (-> (if (directed? flygraph) (digraph) (graph))
      (add-nodes* (nodes flygraph))
      (add-edges* (edges flygraph))))

(defn subsystem-graph
  "Derived graph with subsystems as nodes"
  [g]
  (mapped-by subsystem g))

(defn layer-graph
  "Derived graph with layers as nodes"
  [g]
  (mapped-by layer g))

(defn info "Return type, node and edge count"
  [g]
  [(type g) (count (nodes g)) (count (edges g))]
  )

(defn pkg-filter
  "Return a subgraph of g matching package filter regexp"
  [g regexp]
  (nodes-filtered-by #(re-find regexp (package %)) g))

(defn nodes-filtered-by-regexp
  "Return a subgraph of g matching filter regexp"
  [g regexp]
  (nodes-filtered-by #(re-find regexp (str %)) g))

(defn package-graph
  "Dependency graph with packages as nodes"
  ([g] (mapped-by package g)))

(defn pkg-border-subgraph
  "Return a package graph of types connected to types which belong to packages
  satisfying regexp."
  [g regexp]
  (package-graph (bipartite-subgraph g (filter #(re-find regexp (package %))
                                               (nodes g)))))

(defn pkg-surroundings
  [g regexp]
  (surroundings g (filter #(re-find regexp (package %))
                          (nodes g))))

(deftype JDepGraph [^clojure.lang.IPersistentMap nodemap]
  clojure.lang.Seqable
  (seq [g] (nodes g))

  clojure.lang.Counted
  (count [g] (count (seq g)))

  Graph
  (nodes [g] (keys nodemap))
  (successors* [g node] (if-let [jtypes (get nodemap node)]
                          (imports jtypes)
                          #{}))
  (out-degree [g node] (count (successors g node)))
  (edges [g]
    (for [n1 (nodes g)
          n2 (successors g n1)]
      [n1 n2]))
  (has-node? [g node]
    (contains? nodemap node))
  (has-edge? [g n1 n2]
    (some #{n2} (successors g n1)))

  Digraph)

(defn jgraph
  "Create a dependency graph of all types contained in the seq of jars"
  [jarfiles]
  (let [alltypes (apply concat (map jtypes jarfiles))]
    (->JDepGraph (zipmap (map typename alltypes)
                         alltypes))))


(comment
  (def g
    (jgraph [(-> (get (System/getProperties) "user.home")
                 (str "/.m2/repository/ant/ant/1.6.5/ant-1.6.5.jar"))]))

  (doseq [clazz (extenders Graph)]
    (extend clazz
      clojure.lang.Seqable
      {:seq (fn [g] (nodes g))})))
