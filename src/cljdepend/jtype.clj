(ns ^{:doc
      "Defines JavaType, a type which represents a java type parsed from byte code.
"}
  cljdepend.jtype
  (:require [clojure.reflect :as reflect]
            [clojure.string :as str])
  (:import [clojure.reflect AsmReflector]
           [java.io File InputStream FileInputStream]
           [java.util.jar JarFile JarEntry])
  )

(defprotocol JavaType
  "A protocol abstracting java types"
  (imports [this] "Returns a seq of names of JavaTypes that this imports.")
  (members [this] "Returns a seq of names of JavaTypes that are members or inner types.")
  (dependencies [this]
    "Returns a seq of names of JavaTypes that this depends on. These can be:
  parameter, return or exception types.")
  (^String typename [this] "Returns class or interface name.")
  (^String packagename [this] "Returns package name")
  (^String artifactname [this] "Returns project or jar filename")
  )

(defrecord JType
    [^clojure.lang.Symbol typename
     members                            ; of Symbols
     dependencies                       ; of Symbols
     ^String artifactname
     ]
  JavaType
  (typename [this] (:typename this))
  (artifactname [this] (:artifactname this))

  (imports [this] (concat (:members this) (:dependencies this)))
  (members [this] (:members this))
  (dependencies [this] (:dependencies this))
  )

(defn- classname-for-jarentry [jarentry]
  "Return a symbol which denotes the type contained in the class file."
  (-> (.getName jarentry)
      (str/replace "/" ".")
      (str/replace ".class" "")
      (str/replace-first "WEB-INFes." "") ;; for WARs
      (symbol)))

(extend-protocol reflect/TypeReference
  java.io.File
  (reflect/typename [file]
    (let [path (.toPath file)
          classname (str/replace (str (.getFileName path)) ".class" "")
          ]
      (str (str/join "." (seq (.getParent path))) "." classname)))
  JarEntry
  (reflect/typename [jarentry]
    (reflect/typename (classname-for-jarentry jarentry))
    ))

(defn- complex-type? [symbol]
  "Does symbol denote a complex java type?"
  (let [s (str symbol)]
    (and (some  #(= % \.) s)
         (not (re-find  #"^java\." s)))))

(defn- build-jtype
  "Build a JavaType from the given type-reflect map."
  [tmap declaring-class]
  (let [filter-fn (fn [list-of-types]
                    (set (filter #(and (not= % declaring-class)
                                       (complex-type? %))
                                 list-of-types)))
        members (mapcat
                 (fn [m] (if-let [t (:type m)]
                           (list t) nil))
                 (:members tmap))
        dependencies (concat (mapcat
                              (fn [m]
                                (concat (:parameter-types m)
                                        (:exception-types m)
                                        (if-let [rt (:return-type m)]
                                          (list rt) nil)
                                        (if-let [t (:type m)]
                                          (list t) nil)))
                              (:members tmap))
                             (:bases tmap))]
    (->JType declaring-class
             (filter-fn dependencies)
             (filter-fn members)
             "unknown")))

(defn- reflect-jarentry
  [jarfile jarentry]
  (-> (reflect/type-reflect
       jarentry
       :reflector (AsmReflector. (fn [je] (.getInputStream jarfile je))))
      (build-jtype (classname-for-jarentry jarentry))))

(defn jtypes
  "Search for all java types in jar file and return them as a lazy sequence."
  [jar-file-name]
  (let [f (JarFile. (File. jar-file-name))]
    (map #(assoc (reflect-jarentry f %) :artifactname jar-file-name)
         (filter #(and (not (.isDirectory %))
                       (re-find #"\.class$" (.getName %)))
                 (enumeration-seq (.entries f))))))
