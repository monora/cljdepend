(defproject cljdepend "0.1.0-SNAPSHOT"
  :description "A Clojure library to build dependency graphs of java packages"
  :url "http://"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.1"]
                 [aysylu/loom "0.5.5-SNAPSHOT"]

                 ;; this is only used for test data
                 [ant/ant "1.6.5"]
                 [reply "0.2.0-SNAPSHOT"]
                 ]
  ;; :jvm-opts ["-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5010"]
  ;; :repl-options {
  ;;                ;; If nREPL takes too long to load it may timeout,
  ;;                ;; increase this to wait longer before timing out.
  ;;                ;; Defaults to 30000 (30 seconds)
  ;;                :timeout 1200000
  ;;                }
  ;; :main cljdepend.core
  )
