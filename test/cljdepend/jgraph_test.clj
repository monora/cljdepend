(ns cljdepend.jgraph-test
  (:use clojure.test
        loom.graph
        loom.derived
        loom.io
        loom.derived
        )
  (:use [cljdepend.jgraph] :reload))

(deftest empty-jgraph-test
  (testing "Create an empty jgraph"
    (let [g (jgraph [])]
      (are [expected got] (= expected got)
           true (directed? g)
           true (empty? (nodes g))
           true (empty? (edges g))
           false (has-node? g 1)
           false (has-edge? g 1 2)
           true (empty? (successors g 1))
           0 (out-degree g 1)
           ))))

(defn jgraph-from-m2 [& args]
  (let [m2 (str (get (System/getProperties) "user.home") "/.m2/repository/")]
    (jgraph (map #(str m2  %) args))))


(declare ^:dynamic *testgraph*)
(defn testjar-fixture [f]
  (binding [*testgraph* (jgraph-from-m2 "ant/ant/1.6.5/ant-1.6.5.jar")]
    (f)))

(use-fixtures :once testjar-fixture)

(deftest ant-jgraph-test
  (testing "JDepGraph for ant"
    (let [g *testgraph*
          n1 'org.apache.tools.ant.taskdefs.Ear
          n2 'org.apache.tools.zip.ZipOutputStream]
      (are [expected got] (= expected got)
           576 (count (nodes g))
           1961 (count (edges g))
           true (has-node? g n1)
           n2 (has-edge? g n1 n2)
           nil (has-edge? g n2 n1)
           6 (out-degree g n1)
           4 (out-degree g n2)
           )))
  (testing "package graph"
    (let [g (package-graph *testgraph*)]
      (are [expected got] (= expected got)
           34 (count (nodes g))
           105 (count (edges g))
           ))))

(comment
  ;; Analyse ant
  (def g (jgraph-from-m2 "ant/ant/1.6.5/ant-1.6.5.jar"))
  (view (package-graph g))
  (info g)
  (view (nodes-filtered-by #(re-find #"^org.apache.*ant.*.(taskdefs|tar)" %) (package-graph g)))
  ;; Analyse clojure
  (def g (jgraph-from-m2 "org/clojure/clojure/1.6.0/clojure-1.6.0.jar"))
  (info g)
  (view (nodes-filtered-by #(re-find #"^clojure.asm.commons" (str %)) g))
  (view (border-subgraph g (filter #(re-find #"^clojure.asm" (str %))
                                   (nodes g))))

  )
