(ns cljdepend.jtype-test
  (:use clojure.test
        clojure.pprint)
  (:use [cljdepend.jtype] :reload)
  )


(def ^:dynamic *testjar*
  (-> (get (System/getProperties) "user.home")
      (str "/.m2/repository/ant/ant/1.6.5/ant-1.6.5.jar")))

(def ^:dynamic *testclass* {:typename 'org.apache.tools.ant.AntTypeDefinition,
                            :imports #{'org.apache.tools.ant.Project},
                            :artifactname *testjar*})

(deftest jtype-constructor-test
  (testing "JType constructor"
    (let [t (->JType "name" [] [] "artifactname")]
      (are [expected got] (= expected got)
           "name" (typename t)
           "artifactname" (artifactname t)
           [] (imports t)))))

(deftest jtype-test
  (testing "jar parsing"
    (let [t (first (filter #(= (:typename %)
                               (:typename *testclass*))
                           (jtypes *testjar*)))]
      (is (instance? cljdepend.jtype.JType t))
      (are [expected got] (= expected got)
           (:typename *testclass*) (:typename t) ; redundant
           (:artifactname *testclass*) (:artifactname t)
           (:imports *testclass*) (imports t)
           ))))

(comment
  (def t (first (filter #(= (:typename %)
                            (:typename *testclass*))
                        (jtypes *testjar*)))))
